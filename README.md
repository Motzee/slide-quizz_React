# How it was made ?

Installation of libraries react + redux + thunk
```
npx create-react-app slide-quizz_react
cd slide-quizz_react
npm install --save redux
npm install --save react-redux
npm install --save redux-thunk
```

Creation of project on gitlab (copy url), and add git to the local project :
```
git init
git remote add origin https://gitlab.com/Motzee/slide-quizz_React.git
```
Edition of the .gitignore to add some files to ignore

File tree (simplified) :
slide-quizz_react/
├── public/
│   ├── img/
│   ├── index.html
│   └── manifest.json
├── src/
│   ├── actions/
│   │   └── index_actions.js
│   ├── components/
│   │   └── App.js
│   ├── constants/
│   │   └── ActionTypes.js
│   ├── containers/
│   │   ├── App.css
│   │   └── App.js
│   └── reducers/
│       └── index_reducers.js
├── index.js
└── store.js

