import * as types from "../constants/ActionTypes";

const initialState = {
    questionLoader: false,
    questions: [],
    choices: [],
    showSlide: 1,
    showLoader: true,
    score: 0
};

const questionnaireReducer = (state = initialState, action) => {
    switch (action.type) {

        case types.ADD_QUESTIONS:
            return {
                ...state,
                questions: action.datas
            };
        case types.UPDATE_QUESTION_LOADER:
            return{
                ...state,
                questionLoader: action.trueOrFalse
            };

        case types.CHANGE_SLIDE:
            let currentSlide = parseInt(action.currentSlide, 10);
            return {
                ...state,
                showSlide: parseInt(action.currentSlide, 10) + 1
            };


        case types.UPDATE_CHOICE:
            const filteredChoices = (state.choices.filter(choice => choice.id_question !== action.id_question));
            return {
                ...state,
                choices: [
                    ...filteredChoices,
                    {
                        id_question: action.id_question,
                        id_answer: action.id_answer
                    }
                ]
            };

        case types.UPDATE_SCORE:
            return {
                ...state,
                score: state.score + parseInt(action.points, 10)
            };
            
        case types.SHOW_LOADER:
            return {
                ...state,
                showLoader: action.showLoader
            }
            
        default:
            return state;
    
    }
};
export default questionnaireReducer;