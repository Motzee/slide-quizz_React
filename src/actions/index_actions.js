import * as types from '../constants/ActionTypes' ;

export const fetchQuestions = function(questions){
    return async (dispatch) => {
        dispatch(updateQuestionLoader(true));
        await stall(3000);
        const response = await fetch('http://autodiag.idmkr.io/questions');
        const datas = await response.json();
        dispatch(updateQuestionLoader(false));
        dispatch(addQuestions(datas));
        dispatch(showLoader(false)) ;
        return datas;
    } ;
} ;

export const updateChoice = function(currentSlide, choice, weight, points) {
    return (dispatch) => {
        if(points > 0) {
            let nbPoints = weight * points ;
            dispatch(incrementScore(nbPoints)) ;
        }
        dispatch(memriseChoice(choice)) ;
        dispatch(changeShowSlide(currentSlide)) ;
    } ;
} ;

const updateQuestionLoader = (trueOrFalse) => ({ type: types.UPDATE_QUESTION_LOADER, trueOrFalse: trueOrFalse});
const addQuestions = (datas) => ({ type: types.ADD_QUESTIONS, datas: datas});
const memriseChoice = (choice) => ({ type: types.UPDATE_CHOICE, choice: choice});
const changeShowSlide = (currentSlide) => ({ type: types.CHANGE_SLIDE, currentSlide: currentSlide});
const incrementScore = (nbPoints) => ({ type: types.UPDATE_SCORE, points: nbPoints});
const showLoader = (trueOrFalse) => ({ type: types.SHOW_LOADER, showLoader: trueOrFalse});

//de quoi tester le loader
async function stall(stallTime = 3000) {
    await new Promise(resolve => setTimeout(resolve, stallTime));
}