import { connect } from 'react-redux' ;
import * as actions from '../actions/index_actions' ;
import { bindActionCreators } from 'redux' ;
import App from '../components/App';


const mapStateToProps = state => ({
    questionLoader: state.questionLoader,
    questions: state.questions,
    showLoader: state.showLoader
}) ;


const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
}) ;


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)