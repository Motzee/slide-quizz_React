import { connect } from 'react-redux' ;
import * as actions from '../actions/index_actions' ;
import { bindActionCreators } from 'redux' ;
import Answer from '../components/Answer';


const mapStateToProps = state => ({
    choices: state.choices
}) ;


const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
}) ;


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Answer)