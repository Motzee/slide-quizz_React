import { connect } from 'react-redux' ;
import * as actions from '../actions/index_actions' ;
import { bindActionCreators } from 'redux' ;
import Slide from '../components/Slide';


const mapStateToProps = state => ({
    showSlide: state.showSlide
}) ;


const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
}) ;


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Slide)