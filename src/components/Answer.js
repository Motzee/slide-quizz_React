import React, { Component } from 'react';

class Answer extends Component {
    constructor(props) {
        super(props) ;
    }

    
    render() {
        return (
            <div className="Answer" onClick={this.props.onClick} >
                {this.props.answer.answer}
            </div>
        );
    }
}
export default Answer ;
