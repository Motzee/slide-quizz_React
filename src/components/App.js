import React, { Component } from 'react';
import Slide from '../containers/Slide';


class App extends Component {
    constructor(props) {
        super(props) ;
    }
    
    componentDidMount(){
        this.props.actions.fetchQuestions().then( () =>  {
        //you might want to do something after the questions are loaded
        });
    }
    
    render() {
        if(this.props.showLoader === false) {
            return (
                <div className="App">
                    {this.renderSlides(this.props.questions)}
                    {this.renderSlide(this.result())}
                </div>
            );
        } else {
            return (
                <div className="App">
                    <img src="img/loader.gif" alt="loading : please wait" />
                </div>
            );
        }
    }
    
    renderSlides(questions){
        return questions.map((question) => {
            return this.renderSlide(question) ;
        }) ;
    }
    
    renderSlide(question){
        return <Slide question={question} key={question.id}/>;
    }
    
    result() {
        return {
                "id": 4,
                "question": "Votre score est de :",
                "weight": 1,
                "answers": [
                    {
                        "id": 10,
                        "question_id": 4,
                        "answer": 42,
                        "points": 0
                    }
                ]
            } ;
    }
}
export default App;