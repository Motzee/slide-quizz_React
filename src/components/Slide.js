import React, { Component } from 'react';
import Question from './Question' ;
import Answer from '../containers/Answer' ;

class Slide extends Component {
    constructor(props) {
        super(props) ;
    }
    
    render() {
        return (
            <div className={this.props.showSlide === this.props.question.id ? 'Slide show' : 'Slide hide'}>
    <h1><Question question={ this.props.question.question } /></h1>
                {this.renderAnswers(this.props.question.answers)}
            </div>
        );
    }
    
    
    renderAnswers(answers){
        return answers.map((answer) => {
            return this.renderAnswer(answer) ;
        }) ;
    }
    
    renderAnswer(answer){
        return <Answer answer={answer} key={answer.id} onClick={() => this.props.actions.updateChoice(answer.question_id, answer.id, this.props.question.weight, answer.points)} />;
    }
    
}
export default Slide ;